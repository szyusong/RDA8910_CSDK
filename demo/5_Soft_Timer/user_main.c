/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-21 16:29:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"

//timer  软件定时器


HANDLE timer1 = NULL, timer2 = NULL;
uint8 timer2num = 0;
void timer_handle(void *pParameter)
{
	BOOL err = 0;
	iot_debug_print(pParameter);
	if (strcmp(pParameter, "timer2") == 0)
	{
		timer2num += 1;
		switch (timer2num)
		{
		case 5://当定时器2进入5次后重新创建定时器1，
			//不创建会出现不能启动，证明了单次定时器运行结束会自动停止并删除
			timer1 = OPENAT_create_timer(timer_handle, "timer1");
			while (timer1 == NULL)
			{
				iot_debug_print("timer1 create FALSE");
				osiThreadSleep(1000);
			}
			//启动定时器1，为单次定时器
			err = OPENAT_start_timer0(timer1, 1000 * 10);
			while (!err)
			{
				iot_debug_print("timer1 start FALSE");
				osiThreadSleep(1000);
			}
			break;
		case 10://当定时器2进入10次后停止并删除
			/* 停止定时器接口 */
			OPENAT_stop_timer(timer2);
			/* 删除定时器接口 */
			OPENAT_delete_timer(timer2);
			break;
		default:
			break;
		}
	}
}

static void TestTask(void *param)
{
	//创建两个个定时器
	timer1 = OPENAT_create_timer(timer_handle, "timer1");
	while (timer1 == NULL)
	{
		iot_debug_print("timer1 create FALSE");
		osiThreadSleep(1000);
	}

	timer2 = OPENAT_create_timer(timer_handle, "timer2");
	while (timer2 == NULL)
	{
		iot_debug_print("timer2 create FALSE");
		osiThreadSleep(1000);
	}

	//启动定时器1，为单次定时器
	//单次定时器运行结束后，会自动停止并且删除定时器
	BOOL err = OPENAT_start_timer0(timer1, 1000 * 10);
	while (!err)
	{
		iot_debug_print("timer1 start FALSE");
		osiThreadSleep(1000);
	}

	//启动定时器2，为循环定时器
	err = OPENAT_loop_start_timer(timer2, 1000 * 2);
	while (!err)
	{
		iot_debug_print("timer2 start FALSE");
		osiThreadSleep(1000);
	}
	//线程结束
	osiThreadExit();
}

//main函数
int appimg_enter(void *param)
{
	//系统休眠
	iot_os_sleep(10000);
	//创建一个任务
	osiThreadCreate("TestTask", TestTask, NULL, OSI_PRIORITY_NORMAL, 2048, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
