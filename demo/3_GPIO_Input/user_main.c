/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-20 10:45:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
//GPIO_Input

void gpio_handle(E_OPENAT_DRV_EVT evt, E_AMOPENAT_GPIO_PORT gpioPort, unsigned char state)
{
	uint8 status = 0;
	//iot_debug_print("[gpio] gpio_handle %d,%d,%d", evt, gpioPort, state);
	// 判断是gpio中断
	if (evt == OPENAT_DRV_EVT_GPIO_INT_IND)
	{
		// 判断触发中断的管脚
		if (gpioPort == 0)
		{
			// 触发电平的状态
			iot_debug_print("[gpio] input handle gpio %d, state %d", gpioPort, state);
			// 读当前gpio状态, 1:高电平 0:低电平
			iot_gpio_read(gpioPort, &status);
			iot_debug_print("[gpio] input handle gpio %d, status %d", gpioPort, state);
		}
	}
}

static void TestTask(void *param)
{
	T_AMOPENAT_GPIO_CFG input_cfg = {0};
	BOOL err = 0;
	input_cfg.mode = OPENAT_GPIO_INPUT;					 //配置输入模式
	input_cfg.param.defaultState = 0;					 //默认为低电平
	input_cfg.param.intCfg.debounce = 50;				 //防抖50ms
	input_cfg.param.intCfg.intType = OPENAT_GPIO_NO_INT; //不使用中断
	err = iot_gpio_config(1, &input_cfg);				 //初始化gpio为普通输入模式
	if (!err)
		return;
	iot_debug_print("[gpio] set gpio1 input");

	uint8 value = 0;
	uint8 oldstate = 0;
	while (1)
	{
		//iot_debug_print("[gpio] Tesk Run");
		if (iot_gpio_read(1, &value) == TRUE)
		{
/* 			iot_debug_print("[gpio] input gpio1 ok");
			iot_debug_print("[gpio] value:%d,oldstate:%d", value, oldstate); */
			if (value != oldstate)
			{
				iot_debug_print("[gpio] input gpio1, state %d", value);
				oldstate = value;
			}
		}
		else
		{
			iot_debug_print("[gpio] input gpio1 error");
		}
		//线程休眠500ms
		osiThreadSleep(1000);
	}
	osiThreadExit();
}

//main函数
int appimg_enter(void *param)
{
	T_AMOPENAT_GPIO_CFG input_cfg = {0};
	BOOL err = 0;
	input_cfg.mode = OPENAT_GPIO_INPUT_INT;						//配置输入中断
	input_cfg.param.defaultState = 0;							//默认为低电平
	input_cfg.param.intCfg.debounce = 50;						//防抖50ms
	input_cfg.param.intCfg.intType = OPENAT_GPIO_INT_BOTH_EDGE; //中断触发方式双边沿
	input_cfg.param.intCfg.intCb = gpio_handle;					//中断处理函数
	err = iot_gpio_config(0, &input_cfg);						//初始化gpio为中断输入模式
	if (!err)
		return;
	iot_debug_print("[gpio] set gpio0 INT input");

	//创建一个任务，用作查询法
	osiThreadCreate("TestTask", TestTask, NULL, OSI_PRIORITY_NORMAL, 2048, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
