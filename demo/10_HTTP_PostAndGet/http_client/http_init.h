/*
 * @Author: your name
 * @Date: 2020-05-31 14:31:18
 * @LastEditTime: 2020-05-31 16:06:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\http_client\http_init.h
 */ 
#ifndef APP_HTTP_INIT_H
#define APP_HTTP_INIT_H
#include "http_client.h"

//发送接收缓存区初始化
void http_data_buffer_init(http_client_config_t *config, http_client_handle_t *client);
//剖析URL获取主机名,PORT,文件名和表单
void http_parse_request_url(http_client_config_t *config, http_client_handle_t *client);
//host字段初始化
bool http_host_init(http_client_config_t *config, http_client_handle_t *client);
//port字段初始化
bool http_port_init(http_client_config_t *config, http_client_handle_t *client);
//文件名目录字段初始化
void http_path_init(http_client_config_t *config, http_client_handle_t *client);
//要提交的表单数据
void http_table_init(http_client_config_t *config, http_client_handle_t *client);

//DNS解析ip
bool http_ip_init(http_client_handle_t *client);
//回调函数字段初始化
bool http_event_handler_init(http_client_config_t *config, http_client_handle_t *client);
#endif