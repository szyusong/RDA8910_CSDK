/*
 * @Author: your name
 * @Date: 2020-05-31 14:31:09
 * @LastEditTime: 2020-05-31 16:04:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\http_client\http_init.c
 */
#include <stdlib.h>
#include "string.h"
#include "cs_types.h"
#include "iot_debug.h"
#include "iot_socket.h"
#include "iot_network.h"

#include "http_init.h"
#include "http_client.h"

//发送接收缓存区初始化
void http_data_buffer_init(http_client_config_t *config, http_client_handle_t *client)
{
    //初始化接收数据缓存区
    if (config->buffer_size_rx == 0)
    {
        client->buffer_size_rx = BUFFER_RX;
    }
    else
    {
        client->buffer_size_rx = config->buffer_size_rx;
    }
    client->buffer_rx = iot_os_malloc(client->buffer_size_rx);
    memset(client->buffer_rx, 0, client->buffer_size_rx);

    //初始化发送数据缓存区
    if (config->buffer_size_tx == 0)
    {
        client->buffer_size_tx = BUFFER_TX;
    }
    else
    {
        client->buffer_size_tx = config->buffer_size_tx;
    }
    client->buffer_tx = iot_os_malloc(client->buffer_size_tx);
    memset(client->buffer_tx, 0, client->buffer_size_tx);

    //初始化响应头数据存储区
    client->buffer_respons_header = iot_os_malloc(Respons_Header_len);
    memset(client->buffer_respons_header, 0, Respons_Header_len);

    //初始化响应头状态存储区
    client->respons_state = iot_os_malloc(respons_state_len);
    memset(client->respons_state, 0, respons_state_len);

    //初始化分段传输第一段数据范围
    client->Range_start = 0;
    client->Range_end = client->Range_start + client->buffer_size_rx - 1;
}
//剖析URL获取主机名,PORT,文件名和表单
void http_parse_request_url(http_client_config_t *config, http_client_handle_t *client)
{
    char *PA;
    char *PB;

    PA = config->url;                               //指向url第一位
    if (!strncmp(PA, "http://", strlen("http://"))) //比较指定长度是否一样
    {
        PA = PA + strlen("http://"); //PA的位置指向http://后面
    }
    else if (!strncmp(PA, "https://", strlen("https://")))
    {
        PA = PA + strlen("https://"); //PA的位置指向https://后面
    }
    PB = strchr(PA, '/'); //在一个串中查找给定字符的第一个匹配之处，寻找host结尾处
    if (PB)               //如果PB不为空
    {
        client->host = iot_os_malloc(PB - PA + 1); //为域名分配空间
        memcpy(client->host, PA, PB - PA);         //取出域名
        client->host[PB - PA] = '\0';              //在域名末尾加上\0

        PA = PB; //指向文件名头部
        if (client->method == HTTP_METHOD_GET)
        {
            PB = strchr(PA, '?'); //寻找提交的表单数据
        }
        else
        {
            if (client->method == HTTP_METHOD_POST)
            {
                PB = NULL;
            }
        }
        if (PB) //如果找到了表单数据
        {
            client->path = iot_os_malloc(PB - PA + 1); //为文件名分配空间
            memcpy(client->path, PA, PB - PA);         //取出文件名
            client->path[PB - PA] = '\0';              //在文件名末尾加上\0

            PB = PB + 1;
            client->table_data = iot_os_malloc(strlen(PB) + 1); //为域名分配空间
            memcpy(client->table_data, PB, strlen(PB));         //取出域名
            client->table_data[strlen(PB)] = '\0';              //在域名末尾加上\0
        }
        else
        {
            client->path = iot_os_malloc(strlen(PA)); //为文件名分配空间
            memcpy(client->path, PA, strlen(PA));     //取出文件名
            client->path[strlen(PA)] = '\0';          //在文件名末尾加上\0
        }
    }
    else
    {
        client->host = iot_os_malloc(strlen(PA) + 1);
        memcpy(client->host, PA, strlen(PA));
        client->host[strlen(PA)] = '\0'; //在域名末尾加上\0

        client->path = iot_os_malloc(strlen("/") + 1); //为文件名分配空间
        memcpy(client->path, "/", strlen("/"));        //取出文件名
        client->path[strlen("/")] = '\0';              //在文件名末尾加上\0
    }
    PA = strchr(client->host, ':'); //在一个串中查找给定字符的第一个匹配之处
    if (PA)
    {
        client->port = atoi(PA + 1); //字符串转换成整型数的一个函数  例如：":80",将字符串80转化为数值80
    }
    else
    {
        client->port = 80; //如果域名没有指定端口则默认80端口
    }
}
//host字段初始化
bool http_host_init(http_client_config_t *config, http_client_handle_t *client)
{
    //请求的host
    if (config->host != NULL)
    {
        client->host = iot_os_malloc(strlen(config->host) + 1);
        memcpy(client->host, config->host, strlen(config->host));
        client->host[strlen(config->host)] = '\0';
    }
    else
    {
        //请求的ip
        if (config->ip != NULL)
        {
            client->ip = iot_os_malloc(strlen(config->ip) + 1);
            memcpy(client->ip, config->ip, strlen(config->ip));
            client->ip[strlen(config->ip)] = '\0';
        }
        else
        {
            iot_debug_print("[http_client] Please set url!");
            client->init_state = HTTP_NO_URL;
            http_delete(client);
            return FALSE;
        }
    }
    return TRUE;
}
//port字段初始化
bool http_port_init(http_client_config_t *config, http_client_handle_t *client)
{
    //请求的端口
    if (config->port != 0)
    {
        client->port = config->port;
    }
    else
    {
        iot_debug_print("[http_client] Please set port!");
        client->init_state = HTTP_NO_POST;
        http_delete(client);
        return FALSE;
    }
    return TRUE;
}
//文件名目录字段初始化
void http_path_init(http_client_config_t *config, http_client_handle_t *client)
{
    //请求的文件目录
    if (config->path != NULL)
    {
        if (client->path == NULL)
            client->path = iot_os_malloc(strlen(config->path) + 1);
        memcpy(client->path, config->path, strlen(config->path));
        client->path[strlen(config->path)] = '\0';
    }
    else
    {
        if (client->path == NULL)
            client->path = iot_os_malloc(strlen("/") + 1);
        memcpy(client->path, "/", strlen("/"));
        client->path[strlen("/")] = '\0';
    }
}
//文件名目录字段初始化
void http_table_init(http_client_config_t *config, http_client_handle_t *client)
{
    //附加的表单数据
    if (config->table_data != NULL)
    {
        if (client->table_data == NULL)
            client->table_data = iot_os_malloc(strlen(config->table_data) + 1);
        memcpy(client->table_data, config->table_data, strlen(config->table_data));
        client->table_data[strlen(config->table_data)] = '\0';
    }
}
//DNS解析ip
bool http_ip_init(http_client_handle_t *client)
{
    //DNS解析ip
    if (client->host != NULL)
    {
        struct openat_hostent *hostentP = NULL;
        char *ipAddr = NULL;
        char num = 0;
        while (num < 5)
        {
            hostentP = (struct openat_hostent *)gethostbyname((const char *)client->host);
            if (!hostentP)
            {
                num += 1;
                iot_debug_print("[http_client] gethostbyname %s fail", client->host);
                iot_os_sleep(1000);
            }
            else
            {
                break;
            }
        }
        if (!hostentP)
        {
            iot_debug_print("[http_client] gethostbyname %s fail", client->host);
            client->init_state = HTTP_NO_IP;
            http_delete(client);
            return FALSE;
        }
        // 将ip转换成字符串
        ipAddr = ipaddr_ntoa((const openat_ip_addr_t *)hostentP->h_addr_list[0]);

        client->ip = iot_os_malloc(strlen(ipAddr) + 1);
        memcpy(client->ip, ipAddr, strlen(ipAddr));
        client->ip[strlen(ipAddr)] = '\0';
    }
    return TRUE;
}

//回调函数字段初始化
bool http_event_handler_init(http_client_config_t *config, http_client_handle_t *client)
{
    //事件回调函数
    if (config->event_handler)
    {
        client->event_handler = config->event_handler;
    }
    else
    {
        iot_debug_print("[http_client] Please set event_handler!");
        client->init_state = HTTP_NO_EVENT_HANDLER;
        http_delete(client);
        return FALSE;
    }
    return TRUE;
}
