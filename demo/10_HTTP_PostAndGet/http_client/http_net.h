/*
 * @Author: your name
 * @Date: 2020-05-31 14:40:38
 * @LastEditTime: 2020-05-31 15:41:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\http_client\http_net.h
 */
#ifndef APP_HTTP_NET_H
#define APP_HTTP_NET_H
#include "http_client.h"

#define GET "GET %s HTTP/1.1\r\n"                               \
            "Host: %s\r\n"                                      \
            "Connection: Keep-Alive\r\n"                        \
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) " \
            "AppleWebKit/537.36 (KHTML, like Gecko) "           \
            "Chrome/80.0.3987.163 Safari/537.36\r\n"            \
            "Range: bytes= %s\r\n"                              \
            "Accept: */*\r\n"                                   \
            "\r\n"
#define POST "POST /%s HTTP/1.1\r\n"                             \
             "Host: %s\r\n"                                      \
             "Connection: close\r\n"                             \
             "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) " \
             "AppleWebKit/537.36 (KHTML, like Gecko) "           \
             "Chrome/80.0.3987.163 Safari/537.36\r\n"            \
             "Content-Length: %d\r\n"                            \
             "Content-Type: application/json\r\n"                \
             "Accept: */*\r\n"                                   \
             "\r\n%s"
//get报文生成，支持分段接收，数据缓存区10k
bool http_method_get_update(http_client_handle_t *client);
//对收到的响应头做出解析
bool http_Respons_Header_Analytic(http_client_handle_t *client);
//处理接收数据
bool http_client_recv(http_client_handle_t *client, int socketfd);
//循环发送
bool http_loop_sent(http_client_handle_t *client, int socketfd);
//发送请求任务
void http_client_perform_task(void *param);
#endif