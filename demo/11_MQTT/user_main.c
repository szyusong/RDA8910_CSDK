/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-06-02 11:22:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
//#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"

//#include "http_client.h"
#include "MQTTClient.h"

HANDLE TestTask_HANDLE = NULL;
uint8 NetWorkCbMessage = 0;

#define MQTT_IP "换成自己的ip"	//例如192.168.0.1
#define MQTT_PORT 1883		//自己改端口

Network myNet;
MQTTClient myclient;
unsigned char sendbuf[1024] = {0};
unsigned char recvbuf[1024] = {0};

void MQTT_CB(MessageData *md)
{
	MQTTMessage *message = md->message;
	uint32 datalen = (int)message->payloadlen;

	uint8 data[20] = {0};
	memcpy(data, (char *)message->payload, datalen);

	iot_debug_print("[MQTT_Test] topicName:%s,data:%s,datalen:%d", md->topicName, data, datalen);
}
static void Publish(void *param)
{
	char PubData[20] = {0};
	uint8 num = 0;
	uint8 datalen = 0;
	MQTTMessage message = {0};
	message.qos = QOS0;
	while (1)
	{
		datalen = sprintf(PubData, "RDA8910 Sent:%d", num);
		PubData[datalen] = '\0';
		message.payload = PubData;
		message.payloadlen = datalen;
		MQTTPublish(&myclient, "RDA8910", &message);
		num += 1;
		iot_os_sleep(5000);
	}
}
static void TestTask(void *param)
{
	bool NetLink = FALSE;
	while (NetLink == FALSE)
	{
		T_OPENAT_NETWORK_CONNECT networkparam = {0};
		switch (NetWorkCbMessage)
		{
		case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
			iot_debug_print("[socket] OPENAT_NETWORK_DISCONNECT");
			iot_os_sleep(10000);
			break;
		case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
			iot_debug_print("[socket] OPENAT_NETWORK_READY");
			memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
			//建立网络连接，实际为pdp激活流程
			iot_network_connect(&networkparam);
			iot_os_sleep(500);
			break;
		case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
			iot_debug_print("[socket] OPENAT_NETWORK_LINKED");
			NetLink = TRUE;
			break;
		}
	}
	if (NetLink == TRUE)
	{
		NewNetwork(&myNet);
		ConnectNetwork(&myNet, MQTT_IP, MQTT_PORT);
		MQTTClientInit(&myclient, &myNet, 1000, sendbuf, sizeof(sendbuf), recvbuf, sizeof(recvbuf));
		MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
		data.willFlag = 0;
		data.MQTTVersion = 3;
		data.clientID.cstring = (char *)"RDA8910 MQTT Test";
		data.username.cstring = "Air724UG";
		data.password.cstring = NULL;
		data.keepAliveInterval = 120;
		data.cleansession = 1;
		int rc = MQTTConnect(&myclient, &data);
		iot_debug_print("[MQTT_Test] Connected %d\r\n", rc);
		rc = MQTTSubscribe(&myclient, "FX", QOS1, MQTT_CB);
		iot_debug_print("[MQTT_Test] Subscribed %d\r\n", rc);

		MQTTMessage message = {0};
		message.qos = QOS0;
		message.payload = "RDA8910 Online!";
		message.payloadlen = sizeof("RDA8910 Online!");
		MQTTPublish(&myclient, "RDA8910", &message);
		iot_os_create_task(Publish, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "Publish");
		while (1)
		{
			MQTTYield(&myclient, 500);
			iot_os_sleep(500);
		}
	}
	iot_os_delete_task(TestTask_HANDLE);
}
static void NetWorkCb(E_OPENAT_NETWORK_STATE state)
{
	NetWorkCbMessage = state;
}
//main函数
int appimg_enter(void *param)
{
	//系统休眠
	iot_os_sleep(10000);
	//注册网络状态回调函数
	iot_network_set_cb(NetWorkCb);
	//创建一个任务
	//TestTask_HANDLE =
	TestTask_HANDLE = iot_os_create_task(TestTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "TestTask");
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
