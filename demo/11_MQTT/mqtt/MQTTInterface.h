/*
 * @Author: your name
 * @Date: 2020-05-31 20:53:35
 * @LastEditTime: 2020-06-01 01:40:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\mqtt\TimerInterface.h
 */
#ifndef MQTT_TIMER_INTERFACE_H
#define MQTT_TIMER_INTERFACE_H

typedef struct
{
    unsigned long systick_period;
    unsigned long end_time;
} Timer;

typedef struct Network Network;
struct Network
{
    int my_socket;
    int (*mqttread)(Network *, unsigned char *, int, int);
    int (*mqttwrite)(Network *, unsigned char *, int, int);
    void (*disconnect)(Network *);
};

/*
 * @brief Timer function
 */
/* //启动一个定时器，为mqtt提供系统时基
void Timer_ms_tick_start(void);
//停止定时器
void Timer_ms_tick_stop(void); */
//初始化一个Timer实例
void TimerInit(Timer *);
//返回定时器是否超时
char TimerIsExpired(Timer *);
//设定过多少ms超时
void TimerCountdownMS(Timer *, unsigned int);
//设定过多少s超时
void TimerCountdown(Timer *, unsigned int);
//返回还剩多少ms超时
int TimerLeftMS(Timer *);

/*
 * @brief Network interface porting
 */
int net_read(Network *, unsigned char *, int, int);
int net_write(Network *, unsigned char *, int, int);
void net_disconnect(Network *);
void NewNetwork(Network *n);
int ConnectNetwork(Network *, char *, int);
#endif