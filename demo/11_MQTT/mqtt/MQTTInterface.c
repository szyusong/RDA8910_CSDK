/*
 * @Author: your name
 * @Date: 2020-05-31 20:54:23
 * @LastEditTime: 2020-06-02 00:11:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\mqtt\TimerInterface.c
 */
#include "string.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

//#include "cs_types.h"
#include "iot_debug.h"
#include "iot_socket.h"
#include "iot_network.h"

#include "MQTTInterface.h"

//HANDLE timer1 = NULL;
//unsigned long MilliTimer = 0;
/*
 * @brief MQTT MilliTimer handler
 * @note MUST BE register to your system 1m Tick timer handler.
 */
/* void MilliTimer_Handler(void)
{
    MilliTimer += 5;
}

void Timer_ms_tick_start(void)
{
    //创建两个个定时器
    timer1 = OPENAT_create_timer(MilliTimer_Handler, "timer1");
    if (timer1 == NULL)
    {
        iot_debug_print("[MQTT] Timer create FALSE");
    }
    //启动定时器2，为循环定时器。2ms一次
    BOOL err = OPENAT_loop_start_timer(timer1, 5);
    if (!err)
    {
        iot_debug_print("[MQTT] Timer start FALSE");
    }
}
void Timer_ms_tick_stop(void)
{
    // 停止定时器接口
    OPENAT_stop_timer(timer1);
    // 删除定时器接口 
    OPENAT_delete_timer(timer1);
} */
/*
 * @brief Timer Initialize
 * @param  timer : pointer to a Timer structure
 *         that contains the configuration information for the Timer.
 */
void TimerInit(Timer *timer)
{
    timer->end_time = 0;
}

/*
 * @brief expired Timer
 * @param  timer : pointer to a Timer structure
 *         that contains the configuration information for the Timer.
 */
char TimerIsExpired(Timer *timer)
{
    //long left = timer->end_time - MilliTimer;
    long left = timer->end_time - iot_os_get_system_tick();
    return (left < 0);
}

/*
 * @brief Countdown millisecond Timer
 * @param  timer : pointer to a Timer structure
 *         that contains the configuration information for the Timer.
 *         timeout : setting timeout millisecond.
 */
void TimerCountdownMS(Timer *timer, unsigned int timeout)
{
    //timer->end_time = MilliTimer + timeout;
    timer->end_time = iot_os_get_system_tick() + timeout / 5;
}

/*
 * @brief Countdown second Timer
 * @param  timer : pointer to a Timer structure
 *         that contains the configuration information for the Timer.
 *         timeout : setting timeout millisecond.
 */
void TimerCountdown(Timer *timer, unsigned int timeout)
{
    TimerCountdownMS(timer, timeout * 1000);
}

/*
 * @brief left millisecond Timer
 * @param  timer : pointer to a Timer structure
 *         that contains the configuration information for the Timer.
 */
int TimerLeftMS(Timer *timer)
{
    //long left = timer->end_time - MilliTimer;
    long left = timer->end_time - iot_os_get_system_tick();
    return (left < 0) ? 0 : left;
}

/*
 * @brief New network setting
 * @param  n : pointer to a Network structure
 *         that contains the configuration information for the Network.
 *         sn : socket number where x can be (0..7).
 * @retval None
 */
void NewNetwork(Network *n)
{
    n->my_socket = 0;
    n->mqttread = net_read;
    n->mqttwrite = net_write;
    n->disconnect = net_disconnect;
}

/*
 * @brief read function
 * @param  n : pointer to a Network structure
 *         that contains the configuration information for the Network.
 *         buffer : pointer to a read buffer.
 *         len : buffer length.
 */
int net_read(Network *n, unsigned char *buffer, int len, int timeout_ms)
{
    struct openat_timeval timeVal;
    openat_fd_set fdset;
    int rc = 0;
    int recvLen = 0;
    memset(buffer, '\0', len);
    OPENAT_FD_ZERO(&fdset);
    OPENAT_FD_SET(n->my_socket, &fdset);
    timeVal.tv_sec = 0;
    timeVal.tv_usec = timeout_ms * 1000;
    if (select(n->my_socket + 1, &fdset, NULL, NULL, &timeVal) > 0)
    {
        if (OPENAT_FD_ISSET(n->my_socket, &fdset) > 0) //测试sock是否可读，即是否网络上有数据
        {
            // TCP 接受数据
            do
            {
                rc = recv(n->my_socket, buffer + recvLen, len - recvLen, 0);
                recvLen += rc;
            } while (recvLen < len);
        }
    }
    return recvLen;
}

/*
 * @brief write function
 * @param  n : pointer to a Network structure
 *         that contains the configuration information for the Network.
 *         buffer : pointer to a read buffer.
 *         len : buffer length.
 */
int net_write(Network *n, unsigned char *buffer, int len, int timeout_ms)
{
    // TCP 发送数据
    int sentlen = send(n->my_socket, buffer, len, 0);
    return sentlen;
}

/*
 * @brief disconnect function
 * @param  n : pointer to a Network structure
 *         that contains the configuration information for the Network.
 */
void net_disconnect(Network *n)
{
    close(n->my_socket);
}

/*
 * @brief connect network function
 * @param  n : pointer to a Network structure
 *         that contains the configuration information for the Network.
 *         ip : server iP.
 *         port : server port.
 */
int ConnectNetwork(Network *n, char *ip, int port)
{
    //创建套接字
    n->my_socket = socket(OPENAT_AF_INET, OPENAT_SOCK_STREAM, 0);
    if (n->my_socket < 0)
    {
        iot_debug_print("[MQTT] create tcp socket error");
    }
    else
    {
        // 建立TCP链接
        struct openat_sockaddr_in tcp_server_addr = {0};
        //AF_INET 的目的就是使用 IPv4 进行通信
        tcp_server_addr.sin_family = OPENAT_AF_INET;
        //远端端口，主机字节顺序转变成网络字节顺序
        tcp_server_addr.sin_port = htons((unsigned short)port);
        //字符串远端ip转化为网络序列ip
        inet_aton(ip, &tcp_server_addr.sin_addr);
        int connErr = connect(n->my_socket, (const struct openat_sockaddr *)&tcp_server_addr, sizeof(struct openat_sockaddr));
        if (connErr < 0)
        {
            iot_debug_print("[MQTT] tcp connect error %d", socket_errno(n->my_socket));
            close(n->my_socket);
        }
    }
}
