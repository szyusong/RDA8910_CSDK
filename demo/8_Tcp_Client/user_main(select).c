/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-06-01 23:24:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"

//Tcp Client Demo

#define TCP_SERVER_IP "121.40.198.143"
#define TCP_SERVER_PORT 12415

HANDLE TestTask_HANDLE = NULL;
uint8 NetWorkCbMessage = 0;
int socketfd = -1;

static void SentTask(void *param)
{
	uint8 num = 0;
	int len = 0;
	char data[512] = {0};
	while (1)
	{
		if (socketfd >= 0)
		{
			len = sprintf(data, "RDA8910 Sent:%d", num);
			data[len] = '\0';
			iot_debug_print(data);
			if (len > 0)
			{
				// TCP 发送数据
				len = send(socketfd, data, len + 1, 0);
				if (len < 0)
				{
					iot_debug_print("[socket] tcp send data False");
				}
				else
				{
					iot_debug_print("[socket] tcp send data Len = %d", len);
					num += 1;
				}
			}
		}
		iot_os_sleep(5000);
	}
}

static void RecvTask(void *param)
{
	int len = 0;
	char data[512] = {0};
	while (1)
	{
		if (socketfd >= 0)
		{
			len = 0;
			memset(data, '\0', sizeof(data));

			struct openat_timeval timeVal;
			openat_fd_set fdset;

			OPENAT_FD_ZERO(&fdset);
			OPENAT_FD_SET(socketfd, &fdset);

			timeVal.tv_sec = 0;
			timeVal.tv_usec = 100 * 1000;
			if (select(socketfd + 1, &fdset, NULL, NULL, &timeVal) > 0)
			{
				if (OPENAT_FD_ISSET(socketfd, &fdset) > 0) //测试sock是否可读，即是否网络上有数据
				{
					// TCP 接受数据
					len = recv(socketfd, data, sizeof(data), MSG_DONTWAIT);
					if (len < 0)
					{
						iot_debug_print("[socket] tcp Recv data False");
					}
					else
					{
						iot_debug_print("[socket] tcp Recv data result = %s", data);
					}
				}
			}
		}
		iot_debug_print("[socket] tcp Recv timeout");
		//iot_os_sleep(100);
	}
}
static void TcpConnect()
{
	//创建套接字
	socketfd = socket(OPENAT_AF_INET, OPENAT_SOCK_STREAM, 0);
	while (socketfd < 0)
	{
		iot_debug_print("[socket] create tcp socket error");
		iot_os_sleep(3000);
	}
	// 建立TCP链接
	struct openat_sockaddr_in tcp_server_addr = {0};
	//AF_INET 的目的就是使用 IPv4 进行通信
	tcp_server_addr.sin_family = OPENAT_AF_INET;
	//远端端口，主机字节顺序转变成网络字节顺序
	tcp_server_addr.sin_port = htons((unsigned short)TCP_SERVER_PORT);
	//字符串远端ip转化为网络序列ip
	inet_aton(TCP_SERVER_IP, &tcp_server_addr.sin_addr);
	iot_debug_print("[socket] tcp connect to addr %s", TCP_SERVER_IP);
	int connErr = connect(socketfd, (const struct openat_sockaddr *)&tcp_server_addr, sizeof(struct openat_sockaddr));
	if (connErr < 0)
	{
		iot_debug_print("[socket] tcp connect error %d", socket_errno(socketfd));
		close(socketfd);
	}
	iot_debug_print("[socket] tcp connect success");
	iot_os_create_task(SentTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "SentTask");
	iot_os_create_task(RecvTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "RecvTask");
}
static void TestTask(void *param)
{
	bool NetLink = FALSE;
	while (NetLink == FALSE)
	{
		T_OPENAT_NETWORK_CONNECT networkparam = {0};
		switch (NetWorkCbMessage)
		{
		case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
			iot_debug_print("[socket] OPENAT_NETWORK_DISCONNECT");
			iot_os_sleep(10000);
			break;
		case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
			iot_debug_print("[socket] OPENAT_NETWORK_READY");
			memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
			//建立网络连接，实际为pdp激活流程
			iot_network_connect(&networkparam);
			iot_os_sleep(500);
			break;
		case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
			iot_debug_print("[socket] OPENAT_NETWORK_LINKED");
			NetLink = TRUE;
			break;
		}
	}
	if (NetLink == TRUE)
	{
		TcpConnect();
	}
	iot_os_delete_task(TestTask_HANDLE);
}
static void NetWorkCb(E_OPENAT_NETWORK_STATE state)
{
	NetWorkCbMessage = state;
}
//main函数
int appimg_enter(void *param)
{
	//系统休眠
	iot_os_sleep(10000);
	//注册网络状态回调函数
	iot_network_set_cb(NetWorkCb);
	//创建一个任务
	//TestTask_HANDLE =
	TestTask_HANDLE = iot_os_create_task(TestTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "TestTask");
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
